package com.nespresso.exercise.waiter;

import com.nespresso.exercise.waiter.constants.WaiterConstants;

public class Order {
	
	private String name;
	private int size;

	private String owner;

	public Order(String name, int size, String owner) {
		this.name = name;
		this.size = size;
		this.owner = owner;
	}

	public String getOwner() {
		return this.owner;
	}

	public String getName() {
		return size==1?this.name:this.name+WaiterConstants.FOR_MANY+size;
	}

	public int getSize() {
		return this.size;
	}

}