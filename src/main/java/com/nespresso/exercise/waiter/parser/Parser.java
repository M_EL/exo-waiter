package com.nespresso.exercise.waiter.parser;

import com.nespresso.exercise.waiter.Order;
import com.nespresso.exercise.waiter.constants.WaiterConstants;

public class Parser {

	private static final String RESTO_SEPARATOR = ": ";

	private static final String SAME = "Same";

	/**
	 * 
	 * 
	 * @param input
	 */
	public static Order parseResto(String input, String previous) {
		String[] values = input.split(RESTO_SEPARATOR);
		int size = 1;
		String owner = values[0];
		String name;
		if (values[1].contains(WaiterConstants.FOR_MANY)) {
			String[] values2 = values[1].split(WaiterConstants.FOR_MANY);
			size = Integer.parseInt(values2[1]);
			name = values2[0];
		} else {
			name = values[1];
		}
		if (name.equals(SAME)) {
			name = previous;
		}
		return new Order(name, size, owner);

	}

}
