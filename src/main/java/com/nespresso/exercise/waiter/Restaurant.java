package com.nespresso.exercise.waiter;

import java.util.LinkedHashMap;
import java.util.Map;

import com.nespresso.exercise.waiter.parser.Parser;

public class Restaurant {

	Map<Integer, Table> tableMap = new LinkedHashMap<Integer, Table>();

	public int initTable(int sizeOfTable) {
		int id = tableMap.keySet().size() + 1;
		tableMap.put(id, new Table(sizeOfTable));
		return id;
	}

	public void customerSays(int tableId, String message) {
		Table table = getTable(tableId);
		Order order = Parser.parseResto(message, table.getPrevious());
		table.addOrder(order);
		
	}

	public String createOrder(int tableId) {
		Table table= getTable(tableId);
		if(table.isReady()){
			return table.showCommande();
		}
		return table.showError();
	}
	
	

	private Table getTable(int id) {
		return tableMap.get(id);
	}

}
