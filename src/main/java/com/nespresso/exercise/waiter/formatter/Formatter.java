package com.nespresso.exercise.waiter.formatter;

import java.util.Collection;

import com.nespresso.exercise.waiter.constants.WaiterConstants;

public class Formatter {

	private static final String FORMAT_SEPARATOR = ", ";

	private static final String MISSING = "MISSING ";


	public static String format(Collection<String> inputs) {
		StringBuilder sb = new StringBuilder("");
		int size = inputs.size();
		int i = 1;
		for (String input : inputs) {
			sb.append(input);
			if (i < size) {
				sb.append(FORMAT_SEPARATOR);
			}
			i++;
		}
		return sb.toString();
	}

	public static String showNumberError(int i) {
		return MISSING + i;
	}

	/**
	 * 
	 * MISSING 1 for Fish for 2
	 * 
	 * @param key
	 * @param integer
	 * @return
	 */
	public static String showMultiOrderError(String key, Integer current) {
		int size = Integer.parseInt(key.substring(key.lastIndexOf(" ")+1));
		int missing = size - current;
		return MISSING + missing + WaiterConstants.FOR_MANY + key;

	}

}
