package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.nespresso.exercise.waiter.formatter.Formatter;

public class Table {

	private int numberOfCustomers;
	private Map<String, Order> orders = new LinkedHashMap<String, Order>();

	private Map<String, Integer> multiOrders = new LinkedHashMap<String, Integer>();

	private String previous;

	public Table(int numberOfCustomers) {
		this.numberOfCustomers = numberOfCustomers;
	}

	public void addOrder(Order order) {
		previous = order.getName();
		orders.put(order.getOwner(), order);
		if (order.getSize() > 1) {
			String key = order.getName() ;
			if (multiOrders.containsKey(key)) {
				int currentSize = multiOrders.get(key) + 1;
				if (currentSize == order.getSize()) {
					multiOrders.remove(key);
				}
			} else {
				multiOrders.put(key, 1);
			}
		}
	}

	public boolean isReady() {
		return orders.keySet().size() == numberOfCustomers && multiOrders.size() == 0;
	}

	public String showCommande() {
		List<String> values = new ArrayList<String>();
		for (Order order : orders.values()) {
			values.add(order.getName());
		}
		return Formatter.format(values);
	}

	public String showError() {
		String error = "";
		if (orders.keySet().size() != numberOfCustomers) {
			error = Formatter.showNumberError(numberOfCustomers - orders.keySet().size());
		} else {
			if (!multiOrders.keySet().isEmpty()) {
				for (String key : multiOrders.keySet()) {
					error = Formatter.showMultiOrderError(key, multiOrders.get(key));
				}
			}
		}
		return error;
	}

	public String getPrevious() {
		return this.previous;
	}

}
